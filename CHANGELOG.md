# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.0.1] - 22.08.2021

- Initialization project.

## [0.0.2] - 22.08.2021

### Added
 - configuration CI/CD 
 - Sniffer Code 
 - xdebug
 - configuration codecov

## [0.0.3] - 22.08.2021

### Added
- Product
- UnitPrice
- MinimumQuantity

## [0.0.4] - 22.08.2021

### Added
- Item

## [0.0.5] - 25.08.2021

### Added
- Cart
- Order 
- Tax
- Items
- ItemIterator


