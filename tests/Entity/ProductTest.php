<?php

declare(strict_types=1);

namespace Recruitment\Tests\Entity;

use PHPUnit\Framework\TestCase;
use Recruitment\Entity\Product;

class ProductTest extends TestCase
{
    /**
     * @test
     * @expectedException \Recruitment\Entity\Exception\InvalidUnitPriceException
     */
    public function itThrowsExceptionForInvalidUnitPrice(): void
    {
        $product = new Product();
        $product->setUnitPrice(0);
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     */
    public function itThrowsExceptionForInvalidMinimumQuantity(): void
    {
        $product = new Product();
        $product->setMinimumQuantity(0);
    }

    /**
     * @test
     * @expectedException \Recruitment\Entity\Exception\InvalidTaxRateException
     */
    public function isThrowsExceptionForInvalidTaxRateException(): void
    {
        $product = new Product("F");
    }

    /**
     * @test
     * @expectedException \Recruitment\Entity\Exception\InvalidTaxRateException
     */
    public function isThrowsExceptionForInvalidTaxRateExceptionSetUnitPrice(): void
    {
        $product = new Product("D");
        $product->setUnitPrice(10, "F");
    }
}
