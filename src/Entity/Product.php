<?php
declare(strict_types=1);

namespace Recruitment\Entity;

use Recruitment\Entity\Exception\InvalidUnitPriceException;
use Recruitment\ValueObject\MinimumQuantity;
use Recruitment\ValueObject\TaxRate;
use Recruitment\ValueObject\UnitPrice;

class Product
{

    /**
     * @var int
     */
    private $id = 0;
    /**
     * @var UnitPrice
     */
    private $unitPrice;

    /**
     * @var TaxRate
     */
    private $taxRate;

    /**
     * @var MinimumQuantity
     */
    private $minimumQuantity;

    /**
     * Product constructor.
     * @throws InvalidUnitPriceException
     * @throws Exception\InvalidTaxRateException
     */
    public function __construct(String $ptu = "D")
    {
        $this->unitPrice = new UnitPrice(1);
        $this->minimumQuantity = new MinimumQuantity(1);
        $this->taxRate = new TaxRate($this->getUnitPrice(), $ptu);
    }

    /**
     * @param int $unitPrice
     *
     * @return Product
     * @throws InvalidUnitPriceException
     * @throws Exception\InvalidTaxRateException
     */
    public function setUnitPrice(int $unitPrice, String $ptu = "D"): self
    {
        $this->unitPrice = new UnitPrice($unitPrice);
        $this->taxRate = new TaxRate($this->getUnitPrice(), $ptu);
        return $this;
    }

    /**
     * @param int $minimumQuantity
     *
     * @return Product
     */
    public function setMinimumQuantity(int $minimumQuantity): self
    {
        $this->minimumQuantity = new MinimumQuantity($minimumQuantity);
        return $this;
    }

    /**
     * @param int $id
     *
     * @return Product
     */
    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getUnitPrice(): int
    {
        return $this->unitPrice->getUnitPrice();
    }

    /**
     * @return int
     */
    public function getPriceGross(): int
    {
        return $this->taxRate->getPriceGross();
    }

    /**
     * @return int
     */
    public function getTaxPrice(): int
    {
        return $this->taxRate->getTaxPrice();
    }

    /**
     * @return int
     */
    public function getMinimumQuantity(): int
    {
        return $this->minimumQuantity->getMinimumQuantity();
    }

    public function getPTU() : String
    {
        return $this->taxRate->getPTU();
    }

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }
}
