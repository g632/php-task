<?php


namespace Recruitment\Entity;

use Recruitment\Cart\Cart;

class Order
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var Cart
     */
    private $cart;

    /**
     * Order constructor.
     *
     * @param int $id
     * @param Cart $cart
     */
    public function __construct(int $id, Cart $cart)
    {
        $this->id = $id;
        $this->cart = $cart;
    }


    /**
     * @return array
     */
    public function getDataForView(): array
    {
        $items = [];
        foreach ($this->cart->getItems() as $item) {
            $items[] = [
                'id' => $item->getProduct()->getId(),
                'quantity' => $item->getQuantity(),
                'PTU' => $item->getProduct()->getPTU(),
                'total_price' => $item->getTotalPrice(),
                'total_price_gross' => $item->getTotalPriceGross()
            ];
        }
        return [
            'id' => $this->id,
            'items' => $items,
            'total_price' => $this->cart->getTotalPrice(),
            'total_price_gross' => $this->cart->getTotalPriceGross()
        ];
    }
}
