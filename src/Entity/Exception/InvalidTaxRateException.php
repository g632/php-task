<?php


namespace Recruitment\Entity\Exception;

/**
 * Class InvalidTaxRateException
 * @package Recruitment\Entity\Exception
 */
class InvalidTaxRateException extends \Exception
{
}
