<?php


namespace Recruitment\Entity\Exception;

/**
 * Class InvalidUnitPriceException
 * @package Recruitment\Recruitment\Entity\Exception
 */
class InvalidUnitPriceException extends \Exception
{
}
