<?php
declare(strict_types=1);

namespace Recruitment\Cart;

use Recruitment\Entity\Product;

class Items implements \IteratorAggregate
{

    /**
     * @var array
     */
    private $items = [];


    /**
     * @return array
     */
    public function toArray() : array
    {
        return $this->items;
    }

    /**
     * @param Item $item
     * @throws \Exception
     */
    public function addItem(Item $item) : self
    {
        foreach ($this->getIterator() as $key => $value) {
            if ($item->getProduct()->getId() == $value->getProduct()->getId()) {
                $value->setQuantity($value->getQuantity() + $item->getQuantity());
                $this->items[$key] = $value;
                return $this;
            }
        }
        $this->items[] = $item;
        return $this;
    }

    /**
     * @return int
     */
    public function getTotalPrice() : int
    {
        $totalPrice = 0;
        foreach ($this->getIterator() as $key => $value) {
            $totalPrice += ($value->getProduct()->getUnitPrice() * $value->getQuantity());
        }
        return $totalPrice;
    }

    /**
     * @return int
     */
    public function getTotalPriceGross() : int
    {
        $totalPriceGross = 0;
        foreach ($this->getIterator() as $key => $value) {
            $totalPriceGross += ($value->getProduct()->getPriceGross() * $value->getQuantity());
        }
        return $totalPriceGross;
    }

    /**
     * @return ItemIterator
     */
    public function getIterator(): ItemIterator
    {
        return new ItemIterator($this);
    }

    /**
     * @param \Recruitment\Cart\Item $item
     * @return false
     */
    public function key(Item $item): ?int
    {
        foreach ($this->getIterator() as $key => $value) {
            if ($item->getProduct()->getId() == $value->getProduct()->getId()) {
                return $key;
            }
        }
        return null;
    }

    /**
     * @param Product $product
     * @return bool
     */
    public function offsetUnset(Product $product) : bool
    {
        foreach ($this->getIterator() as $key => $value) {
            if ($value->getProduct()->getId() == $product->getId()) {
                unset($this->items[$key]);
                $this->items = array_values($this->items);
                return true;
            }
        }
        return false;
    }

    /**
     * @param int $offset
     * @return \Recruitment\Cart\Item
     */
    public function offsetGet(int $offset) : Item
    {
        if ($offset < 0 || count($this->toArray()) <= $offset) {
            throw new \OutOfBoundsException();
        }
        return $this->items[$offset];
    }

    /**
     * @param int $offset
     * @param \Recruitment\Cart\Item $value
     */
    public function offsetSet(int $offset, Item $value) : void
    {
        if (is_null($offset)) {
            $this->items[] = $value;
        } else {
            $this->items[$offset] = $value;
        }
    }

    /**
     * @param Product $product
     * @return bool
     */
    public function offsetExists(Product $product) : bool
    {
        foreach ($this->getIterator() as $key => $value) {
            if ($product->getId() == $value->getProduct()->getId()) {
                return true;
            }
        }
        return false;
    }
}
