<?php
declare(strict_types=1);

namespace Recruitment\Cart;

use Recruitment\Entity\Product;
use Recruitment\Cart\ItemsIterator;
use Recruitment\Entity\Order;

class Cart
{

    /**
     * @var Items
     */
    private $items;

    /**
     * Cart constructor.
     */
    public function __construct()
    {
        $this->items = new Items();
    }

    /**
     * @param Product $product
     * @param int $quantity
     * @return $this
     * @throws Exception\QuantityTooLowException
     * @throws \Exception
     */
    public function addProduct(Product $product, int $quantity = 1) : self
    {
        $item = new Item($product, $quantity);
        $this->items->addItem($item);
        return $this;
    }

    /**
     * @throws Exception\QuantityTooLowException
     * @throws \Exception
     */
    public function setQuantity(Product $product, int $quantity) : self
    {
        $item = new Item($product, $quantity);
        if ($this->items->offsetExists($product)) {
            $key = $this->items->key($item);
            $item->setQuantity($quantity);
            $this->items->offsetSet($key, $item);
            return $this;
        }

        $this->items->addItem($item);
        return $this;
    }

    /**
     * @param Product $product
     * @return array
     */
    public function removeProduct(Product $product) : array
    {
        $this->items->offsetUnset($product);
        return $this->items->toArray();
    }

    /**
     * @return int
     */
    public function getTotalPrice(): int
    {
        return $this->items->getTotalPrice();
    }

    /**
     * @return int
     */
    public function getTotalPriceGross(): int
    {
        return $this->items->getTotalPriceGross();
    }

    /**
     * @param int $offset
     * @return Item
     */
    public function getItem(int $offset): Item
    {
        return $this->items->offsetGet($offset);
    }

    /**
     * @return array
     */
    public function getItems(): array
    {
        return $this->items->toArray();
    }

    /**
     * @param int $id
     * @return Order
     */
    public function checkout(int $id): Order
    {
        $cart = clone $this;
        $order = new Order($id, $cart);
        $this->items = new Items();
        return $order;
    }
}
