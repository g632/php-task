<?php


namespace Recruitment\Cart\Exception;

/**
 * Class QuantityTooLowException
 * @package Recruitment\Cart\Exception
 */
class QuantityTooLowException extends \Exception
{
}
