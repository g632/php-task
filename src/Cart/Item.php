<?php
declare(strict_types=1);

namespace Recruitment\Cart;

use phpDocumentor\Reflection\Types\String_;
use Recruitment\Cart\Exception\QuantityTooLowException;
use Recruitment\Entity\Product;

/**
 * Class Item
 * @package Recruitment\Cart
 */
class Item
{

    /**
     * @var Product
     */
    private $product;

    /**
     * @var int
     */
    private $quantity;

    /**
     * Item constructor.
     * @param Product $product
     * @param int $quantity
     * @throws QuantityTooLowException
     */
    public function __construct(Product $product, int $quantity)
    {
        $this->setProduct($product);
        $this->setQuantity($quantity);
    }

    /**
     * @return int
     */
    public function getTotalPrice() : int
    {
        return $this->product->getUnitPrice() * $this->getQuantity();
    }

    /**
     * @return int
     */
    public function getTotalPriceGross(): int
    {
        return $this->product->getPriceGross() * $this->getQuantity();
    }

    /**
     * @param int $quantity
     * @throws QuantityTooLowException
     */
    public function setQuantity(int $quantity): void
    {
        if ($this->product->getMinimumQuantity() > $quantity) {
            throw new QuantityTooLowException();
        }
        $this->quantity = $quantity;
    }

    /**
     * @param Product $product
     */
    public function setProduct(Product $product): void
    {
        $this->product = $product;
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }
}
