<?php


namespace Recruitment\Cart;

use Iterator;

class ItemIterator implements Iterator
{
    /**
     * @var int
     */
    private $position;


    /**
     * @var Items
     */
    private $items;


    /**
     * ItemIterator constructor.
     * @param \Recruitment\Cart\Items $items
     */
    public function __construct(Items $items)
    {
        $this->position = 0;
        $this->items = $items;
    }

    /**
     * @return Item
     */
    public function current() : Item
    {
        return $this->items->toArray()[$this->position];
    }


    /**
     * @return int
     */
    public function key() : int
    {
        return $this->position;
    }


    public function next() : void
    {
        ++$this->position;
    }


    public function rewind() : void
    {
        $this->position = 0;
    }

    /**
     * @return bool
     */
    public function valid() : bool
    {
        return array_key_exists($this->position, $this->items->toArray());
    }
}
