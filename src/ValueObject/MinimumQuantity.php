<?php
declare(strict_types=1);

namespace Recruitment\ValueObject;

final class MinimumQuantity
{
    /**
     * @var int
     */
    private $minimumQuantity;

    /**
     * MinimumQuantity constructor.
     * @param int $minimumQuantity
     */
    public function __construct(int $minimumQuantity)
    {
        if ($this->isValid($minimumQuantity)) {
            $this->minimumQuantity = $minimumQuantity;
        } else {
            throw new \InvalidArgumentException();
        }
    }

    /**
     * @param int $minimumQuantity
     * @return bool
     */
    public function isValid(int $minimumQuantity): bool
    {
        return (bool)$minimumQuantity > 0;
    }

    /**
     * @return int
     */
    public function getMinimumQuantity(): int
    {
        return $this->minimumQuantity;
    }
}
