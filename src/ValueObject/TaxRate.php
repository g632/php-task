<?php


namespace Recruitment\ValueObject;

use Recruitment\Entity\Exception\InvalidTaxRateException;

final class TaxRate
{

    const PTU = array("A" => 1.23, "B" => 1.08, "C" => 1.05, "D" => 1);

    private $ptu;

    private $basedPrice;

    private $taxPrice;

    private $priceGross;

    /**
     * @throws InvalidTaxRateException
     */
    public function __construct(int $price, String $ptu)
    {
        $this->basedPrice = $price;
        if (!array_key_exists($ptu, self::PTU)) {
            throw new InvalidTaxRateException();
        }
        $this->ptu = $ptu;
        $this->priceGross = $price * self::PTU[$ptu];
        $this->taxPrice = $this->priceGross - $this->basedPrice;
    }

    /**
     * @return int
     */
    public function getBasedPrice(): int
    {
        return $this->basedPrice;
    }

    /**
     * @return int
     */
    public function getTaxPrice(): int
    {
        return $this->taxPrice;
    }

    /**
     * @return int
     */
    public function getPriceGross(): int
    {
        return $this->priceGross;
    }

    /**
     * @return String
     */
    public function getPTU() : String
    {
        return $this->ptu;
    }
}
