<?php
declare(strict_types=1);

namespace Recruitment\ValueObject;

use Recruitment\Entity\Exception\InvalidUnitPriceException;

final class UnitPrice
{

    /**
     * @var int
     */
    private $unitPrice;

    /**
     * UnitPrice constructor.
     * @param int $unitPrice
     * @throws InvalidUnitPriceException
     */
    public function __construct(int $unitPrice)
    {
        if ($this->isValid($unitPrice)) {
            $this->unitPrice = $unitPrice;
        } else {
            throw new InvalidUnitPriceException();
        }
    }

    public function isValid(int $unitPrice): bool
    {
        return (bool) $unitPrice > 0;
    }

    /**
     * @return int
     */
    public function getUnitPrice(): int
    {
        return $this->unitPrice;
    }
}
